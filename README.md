# WEB_Calculator

This is a simple calculator that sends to the back-end, there it's processed and the result is given to the front-end.

Dependencies:
```
npm install nodejs express mathjs
```

![](./img/title.png) 