const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const math = require('mathjs');

const app = express();
const PORT = process.env.PORT || 3000;

// Static files
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

let jsonData;

// The route for index.html
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.post('/some-route', (req, res) => {
    try {
        jsonData = req.body; // Get data from request body

        jsonData = math.evaluate(jsonData.dist);
	
    	res.json({ result: jsonData }); // Send the data back to the client in the format JSON
	console.log(jsonData);

    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

app.get('/get-json-data', (req, res) => {
    console.log(jsonData);

    if (jsonData) {
        res.json({ result: jsonData });
    } else {
        res.status(404).json({ error: 'Data not found' });
    }
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
