var display = document.getElementById('display');

var buttons = document.querySelectorAll('[id^="btn_"].tile_ctrl_btn, [id^="btn_"].tile_ctrl_btn_long, [id^="btn_"].tile_ctrl_btn_r, [id^="btn_"].tile_ctrl_btn_n');

buttons.forEach(function(button) {
    button.addEventListener('click', function() {

	if (display.textContent == 0) {
	    display.textContent = button.textContent;
	    if (button.textContent === 'C' || button.textContent === '=')
	        display.textContent = '0';

	    else if (button.textContent === '÷' || button.textContent === '×')
	        display.textContent = '0';
	    else if (button.textContent === '-' || button.textContent === '+')
	        display.textContent = '0';

	    else if (button.textContent === '.' || button.textContent === ')')
	        display.textContent = '0';
	    else if (button.textContent === '%')
	        display.textContent = '0';
	}

	else {
	    if (button.textContent === 'C') {
		if (display.textContent.length == 1)
		    setTimeout(function() {
		        display.textContent = '0';
		        console.log("display.textContent:", display.textContent);
	    	    }, 0);
		else {
		    display.textContent = display.textContent.slice(0, -1);
		}
	    }

	    else if (button.textContent === '=') {
		var dist = display.textContent;

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/some-route", true);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.onreadystatechange = function () {
		    if (xhr.readyState === 4 && xhr.status === 200) {
			console.log("Server response:", xhr.responseText);

		        var data = JSON.parse(xhr.responseText);
            
		        display.textContent = data.result;
		    }
		};
		xhr.send(JSON.stringify({ dist: dist }));
	    }


	    else if (button.textContent === '×') {
		display.textContent += '*';
	    }
	    else if (button.textContent === '÷') {
		display.textContent += '/';
	    }
	
	    else
	        display.textContent += button.textContent;

	}

        if (button.textContent === '=')
	    display.textContent = '0';
    });
});
